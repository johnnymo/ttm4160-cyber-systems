package mqtt;

import runtime.IStateMachine;
import runtime.Scheduler;

import mqtt.MQTTclient;
import sensehat.LEDMatrixTicker;
import runtime.Timer;

class MQTTClientStateMachine implements IStateMachine {
	private MQTTclient client;
	private LEDMatrixTicker ticker;
	private String msg;
	private Timer tick_timer = new Timer("t");
	String EVT_RDY = "LEDMatrixReady",
		   EVT_ERR = "LEDMatrixError", 
		   EVT_WAIT = "LEDMatrixTickerWait", 
		   EVT_FIN = "LEDMatrixTickerFinished",
		   TIMER = "t";
	private enum STATES {INIT, S1, S2, S3, S4, S5, S6, FINAL};
	private STATES state = STATES.INIT;
/*
	public MQTTClientStateMachine(String broker, String id) {
		client = new MQTTclient(broker, id, false, s);
	}
*/
	@Override
	public int fire(String event, Scheduler scheduler) {
		
		if (state == STATES.INIT) {
			if (event.equals("Start")) {
				client = new MQTTclient("tcp://broker.hivemq.com:1883", "oskar2", false, scheduler);
				state = STATES.S1;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S1) {
			if (event.equals("MQTTError")) {
				state = STATES.FINAL;
				return TERMINATE_SYSTEM;
			} else if (event.equals("MQTTReady")) {
				ticker = new LEDMatrixTicker(scheduler);
				state = STATES.S2;
				return EXECUTE_TRANSITION;
			}
			
		} else if (state == STATES.S2) {
			if (event.equals(MQTTclient.CONN_LOST) || event.equals(EVT_ERR)) {
				state = STATES.FINAL;
				return TERMINATE_SYSTEM;
			} else if (event.equals(EVT_RDY)) {
				System.out.println("Subscribing");
				client.subscribe("/led_msg");
				System.out.println("Subscribed");
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			}
			
		} else if (state == STATES.S3) {
			System.out.println("Inside state 3");
			if (event.equals(MQTTclient.CONN_LOST)) {
				state = STATES.FINAL;
				return TERMINATE_SYSTEM;
			} else if (event.equals(MQTTclient.MSG_RCV)) {
				msg = client.getMessage();
				System.out.println(msg);
				ticker.StartWriting(msg);
				
				state = STATES.S4;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S4) {
			System.out.println("Inside state 3");
			if (event.equals(MQTTclient.CONN_LOST)) {
				state = STATES.FINAL;
				return TERMINATE_SYSTEM;
			} else if (event.equals(EVT_ERR)) {
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			} else if (event.equals(EVT_WAIT)) {
				tick_timer.start(scheduler,  1000);
				state = STATES.S5;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S5) {
			System.out.println("Inside state 5");
			if (event.equals(MQTTclient.CONN_LOST) || event.equals(EVT_ERR)) {
				state = STATES.FINAL;
				return TERMINATE_SYSTEM;
			} else if (event.equals(TIMER)) {
				ticker.WritingStep();
				state = STATES.S6;
				return EXECUTE_TRANSITION;
			} else if (event.equals(EVT_FIN)) {
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.S6) {
			System.out.println("Inside state 6");
			if (event.equals(MQTTclient.CONN_LOST) || event.equals(EVT_ERR)) {
				state = STATES.FINAL;
				return TERMINATE_SYSTEM;
			} else if (event.equals(EVT_WAIT)) {
				tick_timer.start(scheduler,  100);
				state = STATES.S5;
				return EXECUTE_TRANSITION;
			} else if (event.equals(EVT_FIN)) {
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.FINAL) {
			System.exit(0);
		}
		
		return DISCARD_EVENT;
	}

	public static void main(String[] args) {
		System.out.println("Initializing MQTT Service");
		MQTTClientStateMachine stm = new MQTTClientStateMachine();
		
		System.out.println("Initializing Scheduler");
		Scheduler s = new Scheduler(stm);
		
		System.out.println("Running system");
		s.addToQueueFirst("Start");
		s.run();
	}

}
