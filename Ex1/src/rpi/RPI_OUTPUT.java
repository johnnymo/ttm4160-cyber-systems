package rpi;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

enum RPI_OUTPUT {
    RED1(RaspiPin.GPIO_07),
    RED2(RaspiPin.GPIO_00),
    YELLOW1(RaspiPin.GPIO_03),
    YELLOW2(RaspiPin.GPIO_12),
    GREEN1(RaspiPin.GPIO_13),
    GREEN2(RaspiPin.GPIO_14),
    BUZZER(RaspiPin.GPIO_10);

    private final Pin pin;

    RPI_OUTPUT(Pin pin) {
        this.pin = pin;
    }

    public Pin getPin() {
        return this.pin;
    }

    public static RPI_OUTPUT[] getPedestreanOutputs() {
        return new RPI_OUTPUT[]{RED1, RED2, GREEN1, GREEN2, BUZZER};
    }

    public static RPI_OUTPUT[] getCarSet() {
        return new RPI_OUTPUT[]{RED1, RED2, YELLOW1, YELLOW2, GREEN1, GREEN2};
    }
}
