package rpi;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.HashMap;
import java.util.Map;


public class TrafficLightsController {

    private static final GpioController GPIO_CONTROLLER = GpioFactory.getInstance();

    private TrafficLightsSTM stm;

    private boolean isPedestrean;

    private final Map<RPI_OUTPUT, GpioPinDigitalOutput> leds = new HashMap<>();

    private GpioPinDigitalInput button;

    public TrafficLightsController(TrafficLightsSTM stm, boolean isPedestrean) {
        this.stm =stm;
        this.isPedestrean = isPedestrean;

        RPI_OUTPUT[] outputs = isPedestrean ? RPI_OUTPUT.getPedestreanOutputs() : RPI_OUTPUT.getCarSet();
        for (RPI_OUTPUT output : outputs) {
            System.out.println("Activating " + output.name() + " via PIN " + output.getPin());
            leds.put(output, GPIO_CONTROLLER.provisionDigitalOutputPin(output.getPin(), output.name(), PinState.LOW));
        }

        this.button = GPIO_CONTROLLER.provisionDigitalInputPin(RaspiPin.GPIO_11, PinPullResistance.PULL_DOWN);
        button.addListener(new GpioPinListenerDigital() {

            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                PinState state = event.getState();
                if (state == PinState.HIGH)
                    TrafficLightsController.this.stm.SCHEDULER.addToQueueLast(TrafficLightsSTM.PEDESTRIAN_BUTTON_PRESSED);
            }
        });

    }

    public void showGreen() {
        for (Map.Entry<RPI_OUTPUT, GpioPinDigitalOutput> output : leds.entrySet()) {
            switch (output.getKey()) {
                case GREEN1:
                case GREEN2: output.getValue().high(); break;
                default: output.getValue().low();
            }
        }
    }

    public void showYellow() {
        for (Map.Entry<RPI_OUTPUT, GpioPinDigitalOutput> output : leds.entrySet()) {
            switch (output.getKey()) {
                case YELLOW1:
                case YELLOW2: output.getValue().high(); break;
                default: output.getValue().low();
            }
        }
    }

    public void showYellowRed() {
        for (Map.Entry<RPI_OUTPUT, GpioPinDigitalOutput> output : leds.entrySet()) {
            switch (output.getKey()) {
                case YELLOW1:
                case YELLOW2:
                case RED1:
                case RED2: output.getValue().high(); break;
                default: output.getValue().low();
            }
        }
    }

    public void showRed() {
        for (Map.Entry<RPI_OUTPUT, GpioPinDigitalOutput> output : leds.entrySet()) {
            switch (output.getKey()) {
                case RED1:
                case RED2: output.getValue().high(); break;
                default: output.getValue().low();
            }
        }
    }

    public void shutdown() {
        for (GpioPinDigitalOutput output : leds.values()) {
            output.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
        }
        this.button.setShutdownOptions(true);
        GPIO_CONTROLLER.shutdown();
    }
}
