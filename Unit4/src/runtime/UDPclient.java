package runtime;

import java.io.IOException;
import java.net.*;


public class UDPclient {
	
	SchedulerData scheduler;
	Thread th;
	Runnable r;
	private DatagramSocket socket;
	private byte[] buf;
	public DatagramPacket packet;
	
	public UDPclient(SchedulerData s) {
		scheduler = s;
	}

	public void send(byte[] buf, InetAddress address, int port) {
		DatagramPacket packet;
		packet = new DatagramPacket(buf, buf.length, address, port);
		try {
			socket.send(packet);
		} catch(IOException e) {
			System.err.println("Can't send packet: " + e);
		}
		
	}
	public void stop() {
		th = null;
		socket.close();
	}
	public void EchoServer(int port) {
		System.out.println("In EchoServer");
		try {
			System.out.println("Trying to listen to port");
			 socket = new DatagramSocket(port);
		 } catch(IOException e) {
			 System.err.println("Error listening UDP socket: " + e);
		 }
	}
	public void listener() {
		 r = new Runnable() {
			@Override
			public void run() {
				while(true) {
					buf = new byte[256];
					packet = new DatagramPacket(buf, buf.length);
					try {
						socket.receive(packet);
						scheduler.addToQueueLast("UDPReception", packet);
						/*
						InetAddress address = packet.getAddress();
			            int port = packet.getPort();
			            packet = new DatagramPacket(buf, buf.length, address, port);
			            String received = new String(packet.getData(), 0, packet.getLength());
			            */
					} catch(IOException e) {
						System.err.println("No packet: " + e);
					}
				}
			}
		};
	th = new Thread(r);
	th.start();
	}
}
