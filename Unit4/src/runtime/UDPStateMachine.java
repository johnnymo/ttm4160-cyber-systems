package runtime;

import java.net.DatagramPacket;
import java.net.InetAddress;


class UDPStateMachine implements IStateMachineData {
	public UDPclient client;

	public int port = 4422;
	private enum STATES {INIT, IDLE, WORKING};
	private STATES state = STATES.INIT;
	@Override
	public int fire(String event, Object object, SchedulerData scheduler) {
		if (state == STATES.INIT) {
			if(event.equals("Start")) {
				System.out.println("In state 1");
				client = new UDPclient(scheduler);
				client.EchoServer(port);
				System.out.println("In state 1 too");
				client.listener();
				state = STATES.IDLE;
				return EXECUTE_TRANSITION;
			}
		} else if (state == STATES.IDLE) {
			if (event.equals("UDPReception")) {
				DatagramPacket packet = (DatagramPacket) object;
				int port = packet.getPort();
				InetAddress address = packet.getAddress();
				byte[] data = packet.getData();
	            client.send(data, address, port);
	            System.out.println("We have received: " + String(data) + " From IP: " + address.toString() + ":" + Integer.toString(port));
			}
		} 
		return 0;
	}
	private String String(byte[] data) {
		// TODO Auto-generated method stub
		return new String(data);
	}
	

	public static void main(String[] args) {
		System.out.println("Initializing UDP Service");
		IStateMachineData stm = new UDPStateMachine();
		
		System.out.println("Initializing Scheduler");
		SchedulerData s = new SchedulerData(stm);
		
		System.out.println("Running system");
		s.addToQueueFirst("Start");
		s.run();
	}
}
